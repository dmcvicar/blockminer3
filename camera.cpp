#include "camera.h"

#include <QDebug>

Camera::Camera( QObject* parent )
    : QObject( parent ),
      m_position( 0.0f, 0.0f, 0.0f ),
      m_upVector( 0.0f, 1.0f, 0.0f ),
      m_forwardVector( 0.0f, 0.0f, 1.0f ),
      m_projectionType( Camera::PerspectiveProjection ),
      m_nearPlane( 0.1f ),
      m_farPlane( 1024.0f ),
      m_fieldOfView( 45.0f ),
      m_aspectRatio( 1.0f ),
      m_left( -0.5 ),
      m_right( 0.5f ),
      m_bottom( -0.5f ),
      m_top( 0.5f ),
      m_viewMatrixDirty( true )
{
    updatePerpectiveProjection();
}

Camera::ProjectionType Camera::projectionType() const
{
    return m_projectionType;
}

QVector3D Camera::position() const
{
    return m_position;
}

void Camera::setPosition( const QVector3D& position )
{
    m_position = position;
    m_viewMatrixDirty = true;
    changed(this);
}

void Camera::setUpVector( const QVector3D& upVector )
{
    m_upVector = upVector;
    m_viewMatrixDirty = true;
    changed(this);
}

QVector3D Camera::upVector() const
{
    return m_upVector;
}

void Camera::setForwardVector(const QVector3D& forwardVector )
{
    m_forwardVector = forwardVector;
    m_viewMatrixDirty = true;
    changed(this);
}

QVector3D Camera::forwardVector() const
{
    return m_forwardVector;
}

QVector3D Camera::leftVector() const
{
    return QVector3D::crossProduct(m_forwardVector, m_upVector);
}

void Camera::setOrthographicProjection( float left, float right,
                                        float bottom, float top,
                                        float nearPlane, float farPlane )
{
    m_left = left;
    m_right = right;
    m_bottom = bottom;
    m_top = top;
    m_nearPlane = nearPlane;
    m_farPlane = farPlane;
    m_projectionType = OrthogonalProjection;
    updateOrthogonalProjection();
    changed(this);
}

void Camera::setPerspectiveProjection( float fieldOfView, float aspectRatio,
                                       float nearPlane, float farPlane )
{
    m_fieldOfView = fieldOfView;
    m_aspectRatio = aspectRatio;
    m_nearPlane = nearPlane;
    m_farPlane = farPlane;
    m_projectionType = PerspectiveProjection;
    updatePerpectiveProjection();
    changed(this);
}

void Camera::setNearPlane( const float& nearPlane )
{
    if ( qFuzzyCompare( m_nearPlane, nearPlane ) )
        return;
    m_nearPlane = nearPlane;
    if (m_projectionType == PerspectiveProjection )
        updatePerpectiveProjection();
    changed(this);
}

float Camera::nearPlane() const
{
    return m_nearPlane;
}

void Camera::setFarPlane( const float& farPlane )
{
    if ( qFuzzyCompare( m_farPlane, farPlane ) )
        return;
    m_farPlane = farPlane;
    if ( m_projectionType == PerspectiveProjection )
        updatePerpectiveProjection();
    changed(this);
}

float Camera::farPlane() const
{
    return m_farPlane;
}

void Camera::setFieldOfView( const float& fieldOfView )
{
    if ( qFuzzyCompare( m_fieldOfView, fieldOfView ) )
        return;
    m_fieldOfView = fieldOfView;
    if ( m_projectionType == PerspectiveProjection )
        updatePerpectiveProjection();
    changed(this);
}

float Camera::fieldOfView() const
{
    return m_fieldOfView;
}

void Camera::setAspectRatio( const float& aspectRatio )
{
    if ( qFuzzyCompare( m_aspectRatio, aspectRatio ) )
        return;
    m_aspectRatio = aspectRatio;
    if ( m_projectionType == PerspectiveProjection )
        updatePerpectiveProjection();
    changed(this);
}

float Camera::aspectRatio() const
{
    return m_aspectRatio;
}

void Camera::setLeft( const float& left )
{
    if ( qFuzzyCompare( m_left, left ) )
        return;
    m_left = left;
    if ( m_projectionType == OrthogonalProjection )
        updateOrthogonalProjection();
    changed(this);
}

float Camera::left() const
{
    return m_left;
}

void Camera::setRight( const float& right )
{
    if ( qFuzzyCompare( m_right, right ) )
        return;
    m_right = right;
    if ( m_projectionType == OrthogonalProjection )
        updateOrthogonalProjection();
    changed(this);
}

float Camera::right() const
{
    return m_right;
}

void Camera::setBottom( const float& bottom )
{
    if ( qFuzzyCompare( m_bottom, bottom ) )
        return;
    m_bottom = bottom;
    if ( m_projectionType == OrthogonalProjection )
        updateOrthogonalProjection();
    changed(this);
}

float Camera::bottom() const
{
    return m_bottom;
}

void Camera::setTop( const float& top )
{

    if ( qFuzzyCompare( m_top, top ) )
        return;
    m_top = top;
    if ( m_projectionType == OrthogonalProjection )
        updateOrthogonalProjection();
    changed(this);
}

float Camera::top() const
{
    return m_top;
}

QMatrix4x4 Camera::viewMatrix() const
{
    if ( m_viewMatrixDirty )
    {
        m_viewMatrix.setToIdentity();
        m_viewMatrix.lookAt( m_position, m_position + m_forwardVector, m_upVector );
        m_viewMatrixDirty = false;
    }
    return m_viewMatrix;
}

QMatrix4x4 Camera::projectionMatrix() const
{
    return m_projectionMatrix;
}

QMatrix4x4 Camera::viewProjectionMatrix() const
{
    return  m_projectionMatrix * viewMatrix();;
}

void Camera::pan( const float& angle )
{
    // rotate forwardVector about upVector
    m_forwardVector = QQuaternion::fromAxisAndAngle(m_upVector, angle).rotatedVector(m_forwardVector);
    m_viewMatrixDirty = true;
    changed( this );
}

void Camera::tilt(const float& angle)
{
    // rotate abought leftVector
    QQuaternion q = QQuaternion::fromAxisAndAngle( leftVector(), angle );
    m_forwardVector = q.rotatedVector( m_forwardVector );
    m_upVector = q.rotatedVector( m_upVector );
    m_viewMatrixDirty = true;
    changed( this );
}

void Camera::roll(const float& angle)
{
    // rotate upVector about forwardVector
    m_upVector = QQuaternion::fromAxisAndAngle(m_forwardVector, angle).rotatedVector(m_upVector);
    m_viewMatrixDirty = true;
    changed( this );
}

void Camera::translate( const QVector3D &translation )
{
    // translate position
    m_position += translation;
    m_viewMatrixDirty = true;
    changed( this );
}

void Camera::rotate( const QMatrix3x3 &rotation )
{
    QQuaternion q = QQuaternion::fromRotationMatrix( rotation );
    m_forwardVector = q.rotatedVector( m_forwardVector );
    m_upVector = q.rotatedVector( m_upVector );
    m_viewMatrixDirty = true;
    changed( this );
}



