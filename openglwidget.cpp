#include "openglwidget.h"

#include <iostream>
#include <QDebug>


OpenGLWidget::OpenGLWidget(QWidget *parent) :
    QOpenGLWidget(parent),
    m_textureImage(0), m_textureNormal(0), m_vertexBuf(QOpenGLBuffer::VertexBuffer),
    m_indexBuf(QOpenGLBuffer::IndexBuffer)
{
    setMinimumSize(500, 400);
}

OpenGLWidget::~OpenGLWidget()
{
    // Make sure the context is current when deleting the texture
    // and the buffers.
    makeCurrent();
    delete m_textureImage;
    delete m_textureNormal;
    doneCurrent();
}

bool OpenGLWidget::prepareShaderProgram( const QString& vertexShaderPath,
                                         const QString& fragmentShaderPath )
{
    // First we load and compile the vertex shader…
    bool result = m_program.addShaderFromSourceFile( QOpenGLShader::Vertex, vertexShaderPath );
    if ( !result )
        qWarning() << m_program.log();

    // …now the fragment shader…
    result = m_program.addShaderFromSourceFile( QOpenGLShader::Fragment, fragmentShaderPath );
    if ( !result )
        qWarning() << m_program.log();

    // …and finally we link them to resolve any references.
    result = m_program.link();
    if ( !result )
        qWarning() << "Could not link shader program:" << m_program.log();

    return result;
}

void OpenGLWidget::initTextures( const QString& imagePath,
                                 const QString& imageNormalPath )
{
    // Load texture image
    m_textureImage = new QOpenGLTexture( QImage( imagePath ).mirrored());

    // Set nearest filtering mode for texture minification
    m_textureImage->setMinificationFilter( QOpenGLTexture::Nearest );

    // Set bilinear filtering mode for texture magnification
    m_textureImage->setMagnificationFilter( QOpenGLTexture::Linear );

    // Wrap texture coordinates by repeating
    // f.ex. texture coordinate (1.1, 1.2) is same as (0.1, 0.2)
    m_textureImage->setWrapMode( QOpenGLTexture::Repeat );

    // Load texture normal map
    m_textureNormal = new QOpenGLTexture( QImage( imageNormalPath ).mirrored());

    // Set nearest filtering mode for texture minification
    m_textureNormal->setMinificationFilter( QOpenGLTexture::Nearest );

    // Set bilinear filtering mode for texture magnification
    m_textureNormal->setMagnificationFilter( QOpenGLTexture::Linear );

    // Wrap texture coordinates by repeating
    // f.ex. texture coordinate (1.1, 1.2) is same as (0.1, 0.2)
    m_textureNormal->setWrapMode( QOpenGLTexture::Repeat );
}


void OpenGLWidget::initializeGL()
{  
    initializeOpenGLFunctions();
    // Generate 2 VBOs
    m_vertexBuf.create();
    m_indexBuf.create();

    //Print out some information about the OpenGL implementation.
    std::cout << "OpenGL Implementation Details:" << std::endl;
    if (this->glGetString(GL_VENDOR))
        std::cout << "\tGL_VENDOR: " << this->glGetString(GL_VENDOR) << std::endl;
    if (this->glGetString(GL_RENDERER))
        std::cout << "\tGL_RENDERER: " << this->glGetString(GL_RENDERER) << std::endl;
    if (this->glGetString(GL_VERSION))
        std::cout << "\tGL_VERSION: " << this->glGetString(GL_VERSION) << std::endl;
    if (this->glGetString(GL_SHADING_LANGUAGE_VERSION))
        std::cout << "\tGL_SHADING_LANGUAGE_VERSION: " << this->glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

    glClearColor(0.2, 0.1, 0, 1);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    // Calculate aspect ratio
    float aspect = float(width()) / float(height() ? height() : 1);
    m_camera.setPerspectiveProjection(45.0f, aspect, 1.0f, 500.0f);

    // ??? kluge
    m_camera.setPosition(QVector3D(0,0,10));
    m_camera.setForwardVector(QVector3D(0,0,-1));

    // Prepare a complete shader program…
    if ( !prepareShaderProgram(  ":/shaders/vshader_line.glsl", ":/shaders/fshader_line.glsl" ) )
        return;

    initTextures(":/textures/gravel.jpg", ":/textures/gravel_norm.jpg");

    m_program.bind();

    // Use QBasicTimer because its faster than QTimer
    timer.start(12, this);
}

void OpenGLWidget::resizeGL(int w, int h)
{
    // Calculate aspect ratio
    float aspect = float(w) / float(h ? h : 1);
    m_camera.setPerspectiveProjection(45.0f, aspect, 1.0f, 500.0f);
}

void OpenGLWidget::paintGL()
{
    // Clear color and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLineWidth(1.5f);

    m_program.setUniformValue("lightPos",0.0,0.0,0.0);
    m_program.setUniformValue("lambient",0.2,0.2,0.2); //setting light property
    m_program.setUniformValue("ldiffuse",0.6,0.6,0.6);
    m_program.setUniformValue("lspecular",0.9,0.9,0.9);
    m_program.setUniformValue("shininess",32);          //shininess

    m_program.setUniformValue("proj_matrix", m_camera.projectionMatrix());
    m_program.setUniformValue("view_matrix", m_camera.viewMatrix());
    m_program.setUniformValue("norm_matrix", m_camera.viewMatrix().normalMatrix());

    m_textureImage->bind(0);
    // Use texture unit 0 which contains texture image
    m_program.setUniformValue("texture", 0);

    m_textureNormal->bind(1);
    // Use texture unit 1 which contains texture normals
    m_program.setUniformValue("texture_normals", 1);\

    // Tell OpenGL which VBOs to use
    m_vertexBuf.bind();
    m_indexBuf.bind();

    ModelData modelData;
    foreach( modelData, m_model.modelDataList() ) {
        if( modelData.visable ) {
            m_program.setUniformValue("mode", modelData.drawingMode);

            // Transfer vertex data to VBO 0
            m_vertexBuf.allocate(modelData.vertices.data(), modelData.vertices.count() * sizeof(VertexData));

            // Transfer index data to VBO 1
            m_indexBuf.allocate(modelData.indices.data(), modelData.indices.count() * sizeof(GLushort));

            // Offset for position
            quintptr offset = 0;

            // Tell OpenGL programmable pipeline how to locate vertex position data
            int vertexLocation = m_program.attributeLocation("a_position");
           // qDebug() << "vertexLocation" << vertexLocation;
            m_program.enableAttributeArray(vertexLocation);
            m_program.setAttributeBuffer(vertexLocation, GL_FLOAT, offset, 3, sizeof(VertexData));

            // Offset for normal coordinate
            offset += sizeof(QVector3D);

            // Tell OpenGL programmable pipeline how to locate vertex normal data
            int normalLocation = m_program.attributeLocation("a_normal");
           // qDebug() << "normalLocation" << normalLocation;
            m_program.enableAttributeArray(normalLocation);
            m_program.setAttributeBuffer(normalLocation, GL_FLOAT, offset, 3, sizeof(VertexData));

            // Offset for texture coordinate
            offset += sizeof(QVector3D);

            // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
            int texcoordLocation = m_program.attributeLocation("a_texcoord");
            m_program.enableAttributeArray(texcoordLocation);
            m_program.setAttributeBuffer(texcoordLocation, GL_FLOAT, offset, 2, sizeof(VertexData));

            // Offset for color coordinate
            offset += sizeof(QVector2D);

            // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
            int colorLocation = m_program.attributeLocation( "a_color" );
            m_program.enableAttributeArray( colorLocation );
            m_program.setAttributeBuffer( colorLocation, GL_FLOAT, offset, 4, sizeof(VertexData) );

            QMatrix4x4 objMatrix;
            foreach( objMatrix, modelData.objMatrices ) {
                m_program.setUniformValue( "obj_matrix", objMatrix );
                // Draw geometry using indices from VBO 1
                switch ( modelData.drawingMode ) {
                case 0:
                    glDrawElements( GL_TRIANGLE_STRIP, modelData.indices.count(), GL_UNSIGNED_SHORT, 0 );
                    break;
                case 1:
                    glDrawElements( GL_LINES, modelData.indices.count(), GL_UNSIGNED_SHORT, 0 );
                    break;
                case 2:
                    glDrawElements( GL_LINE_STRIP, modelData.indices.count(), GL_UNSIGNED_SHORT, 0 );
                    break;
                }
            }
        }
    }
}

void OpenGLWidget::mousePressEvent( QMouseEvent *e )
{
    m_buttonPressed = e->button();
    m_pos = m_prevPos = e->pos();
    if ( m_buttonPressed == Qt::LeftButton )
    {
        //        m_trackball.setRadius(0.4f); // ??? ugly
        //        m_trackball.beginDrag( scaleMouse( m_pos ) );
    }
    e->accept();
}

void OpenGLWidget::mouseReleaseEvent( QMouseEvent *e )
{
    m_buttonPressed = Qt::NoButton;
    m_pos = m_prevPos = e->pos();
    e->accept();
}

void OpenGLWidget::mouseMoveEvent( QMouseEvent *e )
{
    if ( m_buttonPressed == Qt::NoButton )
    {
        e->accept();
        return;
    }
    else {
        m_pos = e->pos();
        if ( m_buttonPressed == Qt::LeftButton )
        {
            // constants to scale dx and dy are arbitrary ... should be calculated
            float dx = 0.2f * ( m_pos.x() - m_prevPos.x() );
            float dy = 0.2f * ( m_pos.y() - m_prevPos.y() );
            m_camera.pan( dx );
            m_camera.tilt( -dy );
        }
        else if ( m_buttonPressed == Qt::MiddleButton )
        {
            // constants to scale dx and dy are arbitrary ... should be calculated
            float dx = 0.2f * ( m_pos.x() - m_prevPos.x() );
            m_camera.roll( dx );

        }
        else if ( m_buttonPressed == Qt::RightButton )
        {
            // constants to scale dx and dy are arbitrary ... should be calculated
            float dx = 0.002f * ( m_pos.x() - m_prevPos.x() );
            float dy = 0.002f * ( m_pos.y() - m_prevPos.y() );
            QVector3D tr = m_camera.leftVector().normalized() * -dx;
            m_camera.translate( tr );
            tr = m_camera.upVector().normalized() * -dy;
            m_camera.translate( tr );
        }
        m_prevPos = m_pos;
        e->accept();
    }
}

void OpenGLWidget::wheelEvent( QWheelEvent *e )
{
    QPoint delta = e->angleDelta();
    // constants to scale delta are arbitrary ... should be calculated
    QVector3D tr = m_camera.forwardVector() * (delta.y() * 0.0002);
    m_camera.translate( tr );
    e->accept();
}

void OpenGLWidget::timerEvent( QTimerEvent * )
{
    // Request an update
    update();
}

















