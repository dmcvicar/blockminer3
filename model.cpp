#include "model.h"


static VertexData s_cubeVertices[24] = {
    // Vertex data for face 0
    {QVector3D(-1.0f, -1.0f,  1.0f), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)},  // v0
    {QVector3D( 1.0f, -1.0f,  1.0f), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.33f, 0.0f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v1
    {QVector3D(-1.0f,  1.0f,  1.0f), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.5f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)},  // v2
    {QVector3D( 1.0f,  1.0f,  1.0f), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.33f, 0.5f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v3

    // Vertex data for face 1
    {QVector3D( 1.0f, -1.0f,  1.0f), QVector3D( 1.0f, 0.0f, 0.0f), QVector2D( 0.0f, 0.5f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v4
    {QVector3D( 1.0f, -1.0f, -1.0f), QVector3D( 1.0f, 0.0f, 0.0f), QVector2D(0.33f, 0.5f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v5
    {QVector3D( 1.0f,  1.0f,  1.0f), QVector3D( 1.0f, 0.0f, 0.0f), QVector2D(0.0f, 1.0f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)},  // v6
    {QVector3D( 1.0f,  1.0f, -1.0f), QVector3D( 1.0f, 0.0f, 0.0f), QVector2D(0.33f, 1.0f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v7

    // Vertex data for face 2
    {QVector3D( 1.0f, -1.0f, -1.0f), QVector3D( 0.0f, 0.0f, -1.0f), QVector2D(0.66f, 0.5f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v8
    {QVector3D(-1.0f, -1.0f, -1.0f), QVector3D( 0.0f, 0.0f, -1.0f), QVector2D(1.0f, 0.5f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)},  // v9
    {QVector3D( 1.0f,  1.0f, -1.0f), QVector3D( 0.0f, 0.0f, -1.0f), QVector2D(0.66f, 1.0f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v10
    {QVector3D(-1.0f,  1.0f, -1.0f), QVector3D( 0.0f, 0.0f, -1.0f), QVector2D(1.0f, 1.0f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)},  // v11

    // Vertex data for face 3
    {QVector3D(-1.0f, -1.0f, -1.0f), QVector3D( -1.0f, 0.0f, 0.0f), QVector2D(0.66f, 0.0f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v12
    {QVector3D(-1.0f, -1.0f,  1.0f), QVector3D( -1.0f, 0.0f, 0.0f), QVector2D(1.0f, 0.0f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)},  // v13
    {QVector3D(-1.0f,  1.0f, -1.0f), QVector3D( -1.0f, 0.0f, 0.0f), QVector2D(0.66f, 0.5f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v14
    {QVector3D(-1.0f,  1.0f,  1.0f), QVector3D( -1.0f, 0.0f, 0.0f), QVector2D(1.0f, 0.5f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)},  // v15

    // Vertex data for face 4
    {QVector3D(-1.0f, -1.0f, -1.0f), QVector3D( 0.0f, -1.0f, 0.0f), QVector2D(0.33f, 0.0f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v16
    {QVector3D( 1.0f, -1.0f, -1.0f), QVector3D( 0.0f, -1.0f, 0.0f), QVector2D(0.66f, 0.0f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v17
    {QVector3D(-1.0f, -1.0f,  1.0f), QVector3D( 0.0f, -1.0f, 0.0f), QVector2D(0.33f, 0.5f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v18
    {QVector3D( 1.0f, -1.0f,  1.0f), QVector3D( 0.0f, -1.0f, 0.0f), QVector2D(0.66f, 0.5f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v19

    // Vertex data for face 5
    {QVector3D(-1.0f,  1.0f,  1.0f), QVector3D( 0.0f, 1.0f, 0.0f), QVector2D(0.33f, 0.5f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v20
    {QVector3D( 1.0f,  1.0f,  1.0f), QVector3D( 0.0f, 1.0f, 0.0f), QVector2D(0.66f, 0.5f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v21
    {QVector3D(-1.0f,  1.0f, -1.0f), QVector3D( 0.0f, 1.0f, 0.0f), QVector2D(0.33f, 1.0f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}, // v22
    {QVector3D( 1.0f,  1.0f, -1.0f), QVector3D( 0.0f, 1.0f, 0.0f), QVector2D(0.66f, 1.0f), QVector4D(1.0f, 1.0f, 1.0f, 1.0f)}  // v23
};

// Indices for drawing cube faces using triangle strips.
// Triangle strips can be connected by duplicating indices
// between the strips. If connecting strips have opposite
// vertex order then last index of the first strip and first
// index of the second strip needs to be duplicated. If
// connecting strips have same vertex order then only last
// index of the first strip needs to be duplicated.
static GLushort s_cubeIndices[34] = {
    0,  1,  2,  3,  3,     // Face 0 - triangle strip ( v0,  v1,  v2,  v3)
    4,  4,  5,  6,  7,  7, // Face 1 - triangle strip ( v4,  v5,  v6,  v7)
    8,  8,  9, 10, 11, 11, // Face 2 - triangle strip ( v8,  v9, v10, v11)
    12, 12, 13, 14, 15, 15, // Face 3 - triangle strip (v12, v13, v14, v15)
    16, 16, 17, 18, 19, 19, // Face 4 - triangle strip (v16, v17, v18, v19)
    20, 20, 21, 22, 23      // Face 5 - triangle strip (v20, v21, v22, v23)
};

static VertexData s_lineVertices[3] = {
    // Vertex data for point 0
    {QVector3D(-3.0f, -3.0f,  1.0f), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(1.0f, 1.0f, 0.0f, 1.0f)},
    // Vertex data for point 1
    {QVector3D( 3.0f, 3.0f, -3.0f), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(0.0f, 1.0f, 1.0f, 1.0f)},

    {QVector3D( 5.0f, 3.0f, 4.0f), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(1.0f, 0.0f, 1.0f, 1.0f)},
};

static GLushort s_lineIndices[3] = {
    0, 1, 2
};

static VertexData s_axisVertices[12] = {
    // x
    {QVector3D(-4.0, 0.0f, 0.0f), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(1.0f, 0.0f, 0.0f, 1.0f)},
    {QVector3D(4.0, 0.0f, 0.0f), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(1.0f, 0.0f, 0.0f, 1.0f)},
    // arrow on positive end of x
    {QVector3D(3.0, 1.0f, 0.0f), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(1.0f, 0.0f, 0.0f, 1.0f)},
    {QVector3D(3.0, -1.0f, 0.0f), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(1.0f, 0.0f, 0.0f, 1.0f)},
    // y
    {QVector3D(0.0, -4.0f, 0.0f), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(0.0f, 1.0f, 0.0f, 1.0f)},
    {QVector3D(0.0, 4.0f, 0.0f), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(0.0f, 1.0f, 0.0f, 1.0f)},
    // arrow on positive end of y
    {QVector3D(1.0, 3.0f, 0.0f), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(0.0f, 1.0f, 0.0f, 1.0f)},
    {QVector3D(-1.0, 3.0f, 0.0f), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(0.0f, 1.0f, 0.0f, 1.0f)},
    // z
    {QVector3D(0.0, 0.0f ,-4.0f ), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(0.0f, 0.0f, 1.0f, 1.0f)},
    {QVector3D(0.0, 0.0f ,4.0f ), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(0.0f, 0.0f, 1.0f, 1.0f)},
    // arrow on positive end of z
    {QVector3D(0.0, 1.0f ,3.0f ), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(0.0f, 0.0f, 1.0f, 1.0f)},
    {QVector3D(0.0, -1.0f ,3.0f ), QVector3D( 0.0f, 0.0f, 1.0f), QVector2D(0.0f, 0.0f), QVector4D(0.0f, 0.0f, 1.0f, 1.0f)},
};

static GLushort s_axisIndices[18] = {
    0, 1,
    1, 2,
    1, 3,
    4, 5,
    5, 6,
    5, 7,
    8, 9,
    9, 10,
    9, 11
};

Model::Model()
{
    QList<QMatrix4x4> objMatrixList;
    QMatrix4x4 objMatrix;

    objMatrixList.clear();
    objMatrix.setToIdentity();
    objMatrix.translate(4, 2, 0);
    objMatrixList.append(objMatrix);
    objMatrix.translate(0, 0, -4);
    objMatrixList.append(objMatrix);
    objMatrix.translate(-5, 0, 0);
    objMatrixList.append(objMatrix);
    append("CUBE", 0, true, sizeof(s_cubeVertices) / sizeof(VertexData), s_cubeVertices, sizeof(s_cubeIndices) / sizeof(GLushort), s_cubeIndices, objMatrixList);

    objMatrixList.clear();
    objMatrix.setToIdentity();
    objMatrixList.append(objMatrix);
    append("LINE", 2, true, sizeof(s_lineVertices) / sizeof(VertexData), s_lineVertices, sizeof(s_lineIndices) / sizeof(GLushort), s_lineIndices, objMatrixList);

    objMatrixList.clear();
    objMatrix.setToIdentity();
    objMatrixList.append(objMatrix);
    append("AXIS", 1, true, sizeof(s_axisVertices) / sizeof(VertexData), s_axisVertices, sizeof(s_axisIndices) / sizeof(GLushort), s_axisIndices, objMatrixList);
}

QMap<QString, ModelData> Model::model() const
{
    return m_model;
}

QList<ModelData> Model::modelDataList() const
{
    return m_model.values();
}

void Model::append(const QString &id, int mode, bool visable, int vertexCount, VertexData *vertexData, int indexCount, GLushort *indexData, const QList<QMatrix4x4> &objMatricies)
{
    ModelData modelData;
    modelData.drawingMode = mode;
    modelData.visable = visable;
    for (int i = 0; i < vertexCount; i++)
        modelData.vertices.append(vertexData[i]);
    for (int i = 0; i < indexCount; i++)
        modelData.indices.append(indexData[i]);
    modelData.objMatrices = objMatricies;
    m_model.insert(id, modelData);
}


