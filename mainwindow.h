#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class OpenGLWidget;
class GeometeryModel;
class ObjectStats;



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionFileExit_triggered();
    void on_actionHelpAbout_triggered();
    void on_actionEditFOV_triggered();
    void on_actionEditNearPlane_triggered();
    void on_actionEditFarPlane_triggered();

private:
    OpenGLWidget *m_openGLWidget;
    GeometeryModel *m_geometeryModel;
    ObjectStats *m_camerastats;

    void createDockWindows();
    void createActions();

    // main menu items
    QMenu *m_viewMenu;
};

#endif // MAINWINDOW_H
