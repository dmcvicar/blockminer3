//#version 330
uniform sampler2D texture;
uniform sampler2D texture_normals;

uniform mat4 view_matrix;
uniform vec3 lightPos;

uniform int mode;

uniform vec3 lambient;	//gl_LightSource[0]
uniform vec3 ldiffuse;
uniform vec3 lspecular;

uniform float shininess;

varying vec3 v_position;
varying vec4 v_color;
varying vec2 v_texcoord;
varying vec3 v_tangentSurface2light;
varying vec3 v_tangentSurface2view;

void main()
{
    float dist = length(v_position-lightPos);	    //distance from light-source to vertex (light position is stationary)
    float att = 1.0/(1.0+0.005*dist+0.001*dist*dist);    //attenuation (constant,linear,quadric)
    if (mode == 0) { // draw surface
        // Set fragment color from texture
        vec3 texcolor = vec3(texture2D(texture,v_texcoord));
        // calc ambient
        vec3 ambient=texcolor*lambient;
        // calc diffuse
        vec3 surf2light=normalize(v_tangentSurface2light);
        vec3 norm=normalize(texture2D(texture_normals,v_texcoord).xyz*2.0-1.0);
        float dcont=min(max(0.0,dot(norm,surf2light)),1.0);
        vec3 diffuse=dcont*(texcolor*ldiffuse); // texcolor added
        // calc specular
        vec3 surf2view=normalize(v_tangentSurface2view);
        vec3 reflection=reflect(-surf2light,norm);
        float scont=pow(max(0.0,dot(surf2view,reflection)),shininess);
        scont=max(min(scont,0.0),1.0); // clamp specular contibution
        vec3 specular=scont*lspecular*texcolor;
        // combine
        gl_FragColor=vec4((ambient+diffuse+specular)*att,1.0);
    } else { // draw line
        gl_FragColor = vec4(v_color.xyz*att, 1.0);
    }
}


