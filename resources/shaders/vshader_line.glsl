//#version 330
uniform mat4 obj_matrix;
uniform mat4 view_matrix;
uniform mat4 proj_matrix;
uniform mat3 norm_matrix;
uniform vec3 lightPos;

uniform int mode;

attribute vec4 a_position;
attribute vec3 a_normal;
attribute vec2 a_texcoord;
attribute vec4 a_color;

varying vec3 v_position;
varying vec2 v_texcoord;
varying vec4 v_color;
varying vec3 v_tangentSurface2light;
varying vec3 v_tangentSurface2view;


void main()
{
    // Calculate vertex position in screen space
    gl_Position = proj_matrix * view_matrix * obj_matrix * a_position;

    // Pass texture coordinate to fragment shader
    // Value will be automatically interpolated to fragments inside polygon faces
    v_texcoord = a_texcoord;

    // Pass color to fragment shader
    v_color = a_color;

    //calc the position of the vertex after translation, rotation, scaling
    v_position=vec3(view_matrix*a_position);

    // calc tangent to normal
    vec3 tangent;
    vec3 v1=cross(a_normal, vec3(0.0,0.0,-1.0)); // reverse signs ??
    vec3 v2=cross(a_normal, vec3(0.0,-1.0,0.0));
    if(length(v1)>length(v2))
        tangent=v1;
    else
        tangent=v2;
    // calc tangent matrix
    vec3 n=normalize(norm_matrix*a_normal);
    vec3 t=normalize(norm_matrix*tangent);
    vec3 b=cross(n,t);
    mat3 mat=mat3(t,b,n);
    // calc tangent transform vectors
    vec3 vector=normalize(lightPos-v_position);
    v_tangentSurface2light=mat*vector;
    vector=normalize(-v_position);
    v_tangentSurface2view=mat*vector;

}

