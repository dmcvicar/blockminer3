#version 330

uniform mat4 proj_matrix;
uniform mat4 view_matrix;
uniform mat4 obj_matrix;

in vec3 a_position;

void main()
{
    gl_Position = proj_matrix * view_matrix * obj_matrix * vec4(a_position, 1.0);
}

