#ifndef PRINTFORMAT_H
#define PRINTFORMAT_H

#include <QString>
#include <QVector3D>
#include <QMatrix4x4>
#include <QMatrix3x3>

class PrintFormat
{
public:
    static QString format(QString s, int v);
    static QString format(QString s, double v);
    static QString format(QString s, const QVector3D &v, bool displayLength = false);
    static QString format(QString s, const QMatrix4x4 &v);
    static QString format(QString s, const QMatrix3x3 &v);

};

#endif // PRINTFORMAT_H
