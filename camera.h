#ifndef CAMERA_H
#define CAMERA_H

#include <QObject>

#include <QMatrix4x4>
#include <QVector3D>


class Camera : public QObject
{
    Q_OBJECT

public:
    Camera( QObject* parent = 0 );

    enum ProjectionType
    {
        OrthogonalProjection,
        PerspectiveProjection
    };

    QVector3D position() const;
    QVector3D upVector() const;
    QVector3D viewCenter() const;
    QVector3D forwardVector() const;
    QVector3D leftVector() const;

    ProjectionType projectionType() const;

    void setOrthographicProjection( float left, float right,
                                    float bottom, float top,
                                    float nearPlane, float farPlane );

    void setPerspectiveProjection( float fieldOfView, float aspect,
                                   float nearPlane, float farPlane );

    void setNearPlane( const float& nearPlane );
    float nearPlane() const;

    void setFarPlane( const float& nearPlane );
    float farPlane() const;

    void setFieldOfView( const float& fieldOfView );
    float fieldOfView() const;

    void setAspectRatio( const float& aspectRatio );
    float aspectRatio() const;

    void setLeft( const float& left );
    float left() const;

    void setRight( const float& right );
    float right() const;

    void setBottom( const float& bottom );
    float bottom() const;

    void setTop( const float& top );
    float top() const;

    QMatrix4x4 viewMatrix() const;
    QMatrix4x4 projectionMatrix() const;
    QMatrix4x4 viewProjectionMatrix() const;

public slots:
    void setPosition( const QVector3D& position );
    void setUpVector( const QVector3D& upVector );
    void setForwardVector( const QVector3D& forwardVector );

    void tilt( const float& angle );
    void pan( const float& angle );
    void roll( const float& angle );
    void translate(const QVector3D& translation);
    void rotate(const QMatrix3x3& rotation);

signals:
    void changed(Camera *camera);

private:
    QVector3D m_position;       // camera position
    QVector3D m_upVector;       // camera up vector
    QVector3D m_forwardVector;  // direction camera is looking at

    Camera::ProjectionType m_projectionType;

    float m_nearPlane;
    float m_farPlane;

    float m_fieldOfView;
    float m_aspectRatio;

    float m_left;
    float m_right;
    float m_bottom;
    float m_top;

    mutable QMatrix4x4 m_viewMatrix;
    mutable QMatrix4x4 m_projectionMatrix;
    mutable QMatrix4x4 m_viewProjectionMatrix;

    mutable bool m_viewMatrixDirty;

    inline void updatePerpectiveProjection()
    {
        m_projectionMatrix.setToIdentity();
        m_projectionMatrix.perspective( m_fieldOfView, m_aspectRatio, m_nearPlane, m_farPlane );
    }

    inline void updateOrthogonalProjection()
    {
        m_projectionMatrix.setToIdentity();
        m_projectionMatrix.ortho( m_left, m_right, m_bottom, m_top, m_nearPlane, m_farPlane );
    }
};

#endif // CAMERA_H
