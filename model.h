#ifndef MODEL_H
#define MODEL_H

#include <QList>
#include <QMap>
#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include <QMatrix4x4>
#include <QOpenGLFunctions>
#include <QColor>

struct VertexData
{
    QVector3D position;
    QVector3D normal;
    QVector2D texCoord;
    QVector4D color;
};

struct ModelData
{
    int drawingMode; // 0 = surface, 1 = lines
    bool visable;
    QVector<VertexData> vertices;
    QVector<GLushort> indices;
    int vertexCount; // dont need
    int indexCount;
    QList<QMatrix4x4> objMatrices;
};

class Model
{
public:
    Model();
    QMap<QString, ModelData> model() const;
    QList<ModelData> modelDataList() const;

    void append(const QString &id, int mode, bool visable, int vertexCount, VertexData *vertexData,
                int indexCount, GLushort *indexData, const QList<QMatrix4x4> &objMatricies);

private:
    QMap<QString, ModelData> m_model;

};

#endif // MODEL_H
