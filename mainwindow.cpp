#include "mainwindow.h"
#include "ui_mainwindow.h"


#include "mainwindow.h"
#include "objectstats.h"
#include "openglwidget.h"



#include <QDockWidget>
#include <QMenuBar>
#include <QSettings>
#include <QMessageBox>
#include <QInputDialog>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    m_openGLWidget = new OpenGLWidget(this);
    m_camerastats = new ObjectStats(this);
    setCentralWidget(m_openGLWidget );
    createActions();
    createDockWindows();

    // update camera stats
    connect(m_openGLWidget->camera(), SIGNAL(changed(Camera*)),
            m_camerastats, SLOT(display(Camera*)));
}

MainWindow::~MainWindow()
{
}

void MainWindow::createActions()
{
     QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
     QMenu *editMenu = menuBar()->addMenu(tr("&Edit"));
     m_viewMenu = menuBar()->addMenu(tr("&View"));
     menuBar()->addSeparator();
     QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));



    QAction *helpAboutAction = new QAction(tr("About"), this);
    helpAboutAction->setStatusTip(tr("Display help about program"));
    connect(helpAboutAction, &QAction::triggered, this, &MainWindow::on_actionHelpAbout_triggered);
    helpMenu->addAction(helpAboutAction);

    QAction *fileExitAction = new QAction(tr("Exit"), this);
    fileExitAction->setStatusTip(tr("Exit this program"));
    connect(fileExitAction, &QAction::triggered, this, &MainWindow::on_actionFileExit_triggered);
    fileMenu->addAction(fileExitAction);

    QAction *editFOVAction = new QAction(tr("Edit FOV..."), this);
    editFOVAction->setStatusTip(tr("Edit the cameras field-of-view"));
    connect(editFOVAction, &QAction::triggered, this, &MainWindow::on_actionEditFOV_triggered);
    editMenu->addAction(editFOVAction);

    QAction *editNearPlaneAction = new QAction(tr("Edit Near Plane..."), this);
    editNearPlaneAction->setStatusTip(tr("Edit the cameras near plane"));
    connect(editNearPlaneAction, &QAction::triggered, this, &MainWindow::on_actionEditNearPlane_triggered);
    editMenu->addAction(editNearPlaneAction);

    QAction *editFarPlaneAction = new QAction(tr("Edit Far Plane..."), this);
    editFarPlaneAction->setStatusTip(tr("Edit the cameras far plane"));
    connect(editFarPlaneAction, &QAction::triggered, this, &MainWindow::on_actionEditFarPlane_triggered);
    editMenu->addAction(editFarPlaneAction);




}

void MainWindow::createDockWindows()
{
    QDockWidget *dockCamera = new QDockWidget(tr("Camera Stats"), this);
    dockCamera->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    dockCamera->setWidget(m_camerastats);
    addDockWidget(Qt::LeftDockWidgetArea, dockCamera);
    m_viewMenu->addAction(dockCamera->toggleViewAction());

}


void MainWindow::on_actionHelpAbout_triggered()
{
    QString str;
    if ( sizeof(void*) == 8 ) {
        str = "64";
    } else if ( sizeof(void*) == 4 ) {
        str = "32";
    } else {
        str = "??";
    }
    QSettings settings;
    QString version = settings.value("AppVersion").toString();
#ifdef QT_DEBUG
    version += " (Debug)";
#endif
    QMessageBox::about(this, tr("BlockMiner3 %1").arg(QChar(169)),
                       tr("<h2>BlockMiner3 %1 Version %2</h2>"
                          "<h3>Qt Library Version "
                          QT_VERSION_STR
                          " (%3bit)</h3>"
                          "<p><b>BlockMiner3 %1</b> this program, is a open pit mine scheduling tool.<p/>"
                          "<p>Use of this program is strictly limited to holders of a valid licence. "
                          "Any  other usage of this program is strictly forbidden.<p/>"
                          "<p>The program is provided AS IS with NO WARRANTY OF ANY KIND, "
                          "INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.<p/>")
                       .arg(QChar(169)).arg(version).arg(str));
}

void MainWindow::on_actionEditFOV_triggered()
{
    bool ok;
    double d = QInputDialog::getDouble(this, tr("Edit Camera FOV"),
                                       tr("Angle (deg):"), m_openGLWidget->camera()->fieldOfView(), 10, 100, 2, &ok);
    if (ok)
        m_openGLWidget->camera()->setFieldOfView((float)d);
}

void MainWindow::on_actionEditNearPlane_triggered()
{
    bool ok;
    double d = QInputDialog::getDouble(this, tr("Edit Camera Near Plane"),
                                       tr("distance:"), m_openGLWidget->camera()->nearPlane(), 0.01, 100000, 2, &ok);
    if (ok)
        m_openGLWidget->camera()->setNearPlane((float)d);
}

void MainWindow::on_actionEditFarPlane_triggered()
{
    bool ok;
    double d = QInputDialog::getDouble(this, tr("Edit Camera Far Plane"),
                                       tr("distance:"), m_openGLWidget->camera()->farPlane(), 0.02, 100000, 2, &ok);
    if (ok)
        m_openGLWidget->camera()->setFarPlane((float)d);
}

void MainWindow::on_actionFileExit_triggered()
{
    close();
}
