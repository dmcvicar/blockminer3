#-------------------------------------------------
#
# Project created by QtCreator 2016-06-06T17:01:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tutorial_01
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    openglwidget.cpp \
    camera.cpp \
    PolyVox/AmbientOcclusionCalculator.inl \
    PolyVox/AStarPathfinder.inl \
    PolyVox/BaseVolume.inl \
    PolyVox/BaseVolumeSampler.inl \
    PolyVox/CubicSurfaceExtractor.inl \
    PolyVox/LowPassFilter.inl \
    PolyVox/MarchingCubesSurfaceExtractor.inl \
    PolyVox/Mesh.inl \
    PolyVox/PagedVolume.inl \
    PolyVox/PagedVolumeChunk.inl \
    PolyVox/PagedVolumeSampler.inl \
    PolyVox/Picking.inl \
    PolyVox/RawVolume.inl \
    PolyVox/RawVolumeSampler.inl \
    PolyVox/Raycast.inl \
    PolyVox/Region.inl \
    PolyVox/Vector.inl \
    PolyVox/VolumeResampler.inl \
    PolyVox/Impl/IteratorController.inl \
    objectstats.cpp \
    printformat.cpp \
    model.cpp

HEADERS  += mainwindow.h \
    openglwidget.h \
    camera.h \
    PolyVox/AmbientOcclusionCalculator.h \
    PolyVox/Array.h \
    PolyVox/AStarPathfinder.h \
    PolyVox/BaseVolume.h \
    PolyVox/Config.h \
    PolyVox/CubicSurfaceExtractor.h \
    PolyVox/DefaultIsQuadNeeded.h \
    PolyVox/DefaultMarchingCubesController.h \
    PolyVox/Density.h \
    PolyVox/Exceptions.h \
    PolyVox/FilePager.h \
    PolyVox/Logging.h \
    PolyVox/LowPassFilter.h \
    PolyVox/MarchingCubesSurfaceExtractor.h \
    PolyVox/Material.h \
    PolyVox/MaterialDensityPair.h \
    PolyVox/Mesh.h \
    PolyVox/PagedVolume.h \
    PolyVox/Picking.h \
    PolyVox/RawVolume.h \
    PolyVox/Raycast.h \
    PolyVox/Region.h \
    PolyVox/Vector.h \
    PolyVox/Vertex.h \
    PolyVox/VolumeResampler.h \
    PolyVox/Impl/AStarPathfinderImpl.h \
    PolyVox/Impl/ErrorHandling.h \
    PolyVox/Impl/ExceptionsImpl.h \
    PolyVox/Impl/Interpolation.h \
    PolyVox/Impl/IteratorController.h \
    PolyVox/Impl/LoggingImpl.h \
    PolyVox/Impl/MarchingCubesTables.h \
    PolyVox/Impl/Morton.h \
    PolyVox/Impl/PlatformDefinitions.h \
    PolyVox/Impl/RandomUnitVectors.h \
    PolyVox/Impl/RandomVectors.h \
    PolyVox/Impl/Timer.h \
    PolyVox/Impl/Utility.h \
    PolyVox/Impl/Assertions.h \
    objectstats.h \
    printformat.h \
    model.h

FORMS    +=

#LIBS += -lglut -lGLU

RESOURCES += \
    resources.qrc

DISTFILES +=
