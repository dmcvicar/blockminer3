#include "objectstats.h"
#include "camera.h"
#include "printformat.h"

#include <QHBoxLayout>
#include <QPlainTextEdit>


//#include <QDebug>

ObjectStats::ObjectStats(QWidget *parent) : QWidget(parent)
{
    setMinimumWidth(500);
    m_text = new QPlainTextEdit(this);
    m_text->setReadOnly(true);
    QFont font;
    font.setFamily("Courier New");
    m_text->setFont(font);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(m_text);
    setLayout(layout);
}

void ObjectStats::display(Camera *camera)
{
    m_text->clear();
    // view volume
    if (camera->projectionType() == Camera::OrthogonalProjection){
        m_text->appendPlainText("OrthogonalProjection");
        m_text->appendPlainText(PrintFormat::format("left", camera->left()));
        m_text->appendPlainText(PrintFormat::format("right", camera->right()));
        m_text->appendPlainText(PrintFormat::format("bottom", camera->bottom()));
        m_text->appendPlainText(PrintFormat::format("top", camera->top()));
    } else {
        m_text->appendPlainText("PerspectiveProjection");
        m_text->appendPlainText(PrintFormat::format("fov", camera->fieldOfView()));
        m_text->appendPlainText(PrintFormat::format("aspect", camera->aspectRatio()));
    }
    m_text->appendPlainText(PrintFormat::format("near", camera->nearPlane()));
    m_text->appendPlainText(PrintFormat::format("far", camera->farPlane()));
    m_text->appendPlainText("");
 //   m_text->appendPlainText(PrintFormat::format("projectionMatrix", camera->projectionMatrix()));
    // camera position and vectors
    m_text->appendPlainText(PrintFormat::format("position", camera->position()));
    m_text->appendPlainText(PrintFormat::format("upVector", camera->upVector()));
    m_text->appendPlainText(PrintFormat::format("forwardVec", camera->forwardVector()));
  //  m_text->appendPlainText(PrintFormat::format("up dot fwd", QVector3D::dotProduct(camera->upVector(),camera->forwardVector())));

   // m_text->appendPlainText(PrintFormat::format("leftVector", camera->leftVector()));
    m_text->appendPlainText("");
    // matrices
    m_text->appendPlainText(PrintFormat::format("viewMatrix", camera->viewMatrix()));
//    m_text->appendPlainText("");
//    m_text->appendPlainText(PrintFormat::format("projectionMatrix", camera->projectionMatrix()));
//    m_text->appendPlainText("");
//    m_text->appendPlainText(PrintFormat::format("viewProjectionMatrix", camera->viewProjectionMatrix()));
//    m_text->appendPlainText(PrintFormat::format("normMatrix", camera->viewMatrix().normalMatrix()));
}

