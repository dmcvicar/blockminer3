#ifndef OPENGLWIDGET_H
#define OPENGLWIDGET_H

#include <QObject>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLBuffer>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QBasicTimer>

#include "camera.h"
#include "model.h"

class OpenGLWidget: public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    OpenGLWidget(QWidget *parent);
    ~OpenGLWidget();

    Camera *camera() { return &m_camera; }




protected:
    virtual void initializeGL();
    virtual void resizeGL( int w, int h );
    virtual void paintGL();

    virtual void mousePressEvent( QMouseEvent* e );
    virtual void mouseReleaseEvent( QMouseEvent* e );
    virtual void mouseMoveEvent( QMouseEvent* e );
    virtual void wheelEvent( QWheelEvent *e );
    virtual void timerEvent(QTimerEvent *e );

private:
    QBasicTimer timer;
    Camera m_camera;
    Qt::MouseButton m_buttonPressed;
    QPoint m_prevPos;
    QPoint m_pos;

    QOpenGLShaderProgram m_program;
    QOpenGLTexture *m_textureImage;
    QOpenGLTexture *m_textureNormal;
    QOpenGLBuffer m_vertexBuf;
    QOpenGLBuffer m_indexBuf;

    // hard coded for now, expose later
    bool prepareShaderProgram(const QString &vertexShaderPath, const QString &fragmentShaderPath);
    void initTextures(const QString &imagePath, const QString &imageNormalPath);

    Model m_model;


};

#endif // OPENGLWIDGET_H
