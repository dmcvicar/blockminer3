#include "printformat.h"

static const int labelWidth = 12;
static const int numericWidth = 11;


QString PrintFormat::format(QString s, int v)
{
    return QString("%1 %2")
            .arg(s.leftJustified(labelWidth))
            .arg(v, numericWidth);
}

QString PrintFormat::format(QString s, double v)
{
    return QString("%1 %2")
            .arg(s.leftJustified(labelWidth))
            .arg(v, numericWidth);
}

QString PrintFormat::format(QString s, const QVector3D &v, bool displayLength)
{
    if(displayLength)
        return QString("%1 %2  %3  %4 %5")
                .arg(s.leftJustified(labelWidth))
                .arg(v.x(), numericWidth)
                .arg(v.y(), numericWidth)
                .arg(v.z(), numericWidth)
                .arg(v.length(), numericWidth);
    else
        return QString("%1 %2  %3  %4")
                .arg(s.leftJustified(labelWidth))
                .arg(v.x(), numericWidth)
                .arg(v.y(), numericWidth)
                .arg(v.z(), numericWidth);
}

QString PrintFormat::format(QString s, const QMatrix4x4 &v)
{
    QString str = QString("%1\n")
            .arg(s.leftJustified(labelWidth));
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            str +=  QString("%1  ").arg(v(i,j), numericWidth);
        }
        if (i < 3)
            str += "\n";
    }
    return str;
}

QString PrintFormat::format(QString s, const QMatrix3x3 &v)
{
    QString str = QString("%1\n")
            .arg(s.leftJustified(labelWidth));
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            str +=  QString("%1  ").arg(v(i,j), numericWidth);
        }
        if (i < 3)
            str += "\n";
    }
    return str;
}
