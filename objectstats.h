#ifndef CAMERASTATS_H
#define CAMERASTATS_H

#include <QWidget>


class QPlainTextEdit;
class Camera;
class Trackball;

class ObjectStats : public QWidget
{
    Q_OBJECT
public:
    explicit ObjectStats(QWidget *parent = 0);

public slots:
    void display(Camera *camera);

private:
    QPlainTextEdit *m_text;
};

#endif // CAMERASTATS_H
