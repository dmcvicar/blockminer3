#include "mainwindow.h"
#include <QApplication>
#include <QSettings>
#include <QSurfaceFormat>

//static const VersionStack AppVersion("0.0.1");

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("MCVCorp");
    QCoreApplication::setOrganizationDomain("mcvcorp.com");
    QCoreApplication::setApplicationName("BlockMiner3");

    QApplication a(argc, argv);

    QSettings settings;
    settings.setValue("AppVersion", "0.0.0");

//    QSurfaceFormat format;
//    format.setDepthBufferSize(24);
//    QSurfaceFormat::setDefaultFormat(format);

    QSurfaceFormat format;
    format.setDepthBufferSize( 24 );
    format.setRenderableType( QSurfaceFormat::OpenGL );
    format.setProfile( QSurfaceFormat::CompatibilityProfile );
    format.setSwapBehavior(QSurfaceFormat::DoubleBuffer);
    format.setVersion( 2, 0 );
    QSurfaceFormat::setDefaultFormat( format );

    MainWindow w;
    w.setWindowIcon( QIcon( ":/icons/resources/icons/BlockMiner.ico" ) );
    w.show();

    return a.exec();
}
